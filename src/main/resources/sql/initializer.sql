/**
 * Author:  Froy
 * Created: 5/04/2020
 * Version 1.4
 */

/**
  Role creation to DB conection.
 */
CREATE USER oxygen WITH PASSWORD '19900409';

/**
  Create database and schemas for DB.
 */
CREATE DATABASE oxygen WITH OWNER oxygen;
CREATE SCHEMA financial;
CREATE SCHEMA config;

/**
  Create tables for
 */
create table financial.products
(
	id int not null
		constraint products_pk
			primary key,
	key varchar(50) not null,
	name varchar(100) not null
);

create unique index products_key_uindex
	on financial.products (key);
--Sequence
create sequence financial.products_id_seq;

alter table financial.products alter column id set default nextval('financial.products_id_seq');

alter sequence financial.products_id_seq owned by financial.products.id;

--Se agrega la tabla pricelist
create table financial.pricelist
(
	id serial not null
		constraint pricelist_pk
			primary key,
	fk_product_id int not null
		constraint pricelist_products_id_fk
			references financial.products,
	price decimal(4,2)
);

--Sequence
create sequence financial.pricelist_id_seq;

alter table financial.pricelist alter column id set default nextval('financial.pricelist_id_seq');

alter sequence financial.pricelist_id_seq owned by financial.pricelist.id;

--Drop Constraint for foreign key
--Alterar la restricción de suprimir registrar
ALTER TABLE pricelist
  DROP CONSTRAINT pricelist_products_id_fk;
ALTER TABLE pricelist ADD FOREIGN KEY (fk_product_id)
    REFERENCES products (id)
    ON DELETE CASCADE;

--Tabla Stock
create table financial.stock
(
	id serial not null
		constraint stock_pk
			primary key,
	fk_product_id int not null
		constraint stock_products_id_fk
			references financial.products
				on delete cascade,
	count int not null
);

alter table financial.stock alter column id set default nextval('financial.stock_id_seq');

alter sequence financial.stock_id_seq owned by financial.stock.id;
/**
    Se agrega un nueva columna
    Since initializer version 1.4
*/
alter table financial.stock
	add location varchar(20);