package info.froylanvillaverde.oxygen.financial.controller;

import info.froylanvillaverde.oxygen.config.model.MessageDto;
import info.froylanvillaverde.oxygen.financial.model.Product;
import info.froylanvillaverde.oxygen.financial.service.ProductService;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

/**
 *
 * @author Froylan Villaverde Gonzalez
 * @since 0.0.1
 * @version 1.2
 */
@Controller
public class ProductController {
    
    private static Logger log = LoggerFactory.getLogger(ProductController.class);
    
    @Autowired
    private ProductService productService;
    
    @GetMapping("/product/list")
    public String listProducts(Model model){
        
        model.addAttribute("products",productService.listProducts());
        return "financial/products";
    }
    
    @GetMapping("/product/add")
    public String addProduct(Product product, Model model){
        model.addAttribute("product",new Product());
        return "financial/productForm";
    }
    
    @GetMapping("/product/edit/{id}")
    public String editProduct(Model model, Product product){

        product = productService.findProduct(product);
        model.addAttribute("product", product);
        return "financial/productForm";
    }
    
    @PostMapping("/product/save")
    public String saveProduct(@Valid Product product, Errors errors, Model model){
        List<MessageDto> messages = new ArrayList<>();
        if (errors.hasErrors()) {
            return "financial/productForm";
        }
        try {
            productService.save(product);
        } catch (Exception e) {
            e.printStackTrace();
            messages.add(new MessageDto(e.getCause().toString(), MessageDto.Type.DANGER));
            model.addAttribute("messages", messages);
            return listProducts(model);
        }        
        messages.add(new MessageDto("Product created successfully", MessageDto.Type.SUCCESS));
        model.addAttribute("messages",messages);
        return listProducts(model);
    }
    
    @GetMapping("/product/delete")
    public String deleteProduct(Product product, Model model){
        List<MessageDto> messages = new ArrayList<>();
        messages.add(new MessageDto("Product removed successfully", MessageDto.Type.SUCCESS));
        model.addAttribute("messages",messages);
        productService.delete(product);
        return listProducts(model);
    }
}
