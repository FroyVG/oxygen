package info.froylanvillaverde.oxygen.financial.controller;

import info.froylanvillaverde.oxygen.financial.service.PricelistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 *
 * @author Froylan Villaverde Gonzalez
 * @since 0.0.2
 * @version 1.0
 */
public class PricelistController {
    
    @Autowired
    private PricelistService pricelistService;
    
    @GetMapping("/pricelist/list")
    public String listPricelist(Model model){
        model.addAttribute("pricelist", pricelistService.listPricelist());
        return "financial/priceList";
    }
    
}
