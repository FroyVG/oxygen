/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.froylanvillaverde.oxygen.financial.service;

import info.froylanvillaverde.oxygen.financial.model.Pricelist;
import java.util.List;

/**
 *
 * @author Froylan Villaverde Gonzalez
 * @since 0.0.2
 * @version 1.0
 */
public interface PricelistService {
    
    public List<Pricelist> listPricelist();
    
    public void save(Pricelist pricelist);
    
    public void delete(Pricelist pricelist);
    
    public Pricelist findPricelist(Pricelist pricelist);
}
