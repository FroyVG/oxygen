package info.froylanvillaverde.oxygen.financial.service;

import info.froylanvillaverde.oxygen.financial.dao.ProductDao;
import info.froylanvillaverde.oxygen.financial.model.Product;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Froylan Villaverde Gonzalez
 * @since 0.0.1
 * @version 1.1
 */
@Service
public class ProductServiceImpl implements ProductService{
    
    private static Logger logger = LoggerFactory.getLogger(ProductServiceImpl.class);
    
    @Autowired
    private ProductDao productDao;

    @Override
    @Transactional(readOnly = true)
    public List<Product> listProducts() {
        return (List<Product>)productDao.findAll();
    }

    @Override
    @Transactional
    public void save(Product product) {
        logger.info(product.getPricelist().toString() + ", " + product);
        product.getPricelist().setPrice(product.getPricelist().getPrice());
        productDao.save(product);
    }

    @Override
    @Transactional
    public void delete(Product product) {
        productDao.delete(product);
    }

    @Override
    @Transactional(readOnly = true)
    public Product findProduct(Product product) {
        return productDao.findById(product.getId()).orElse(null);
    }
    
}
