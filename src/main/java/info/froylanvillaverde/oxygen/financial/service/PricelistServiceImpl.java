package info.froylanvillaverde.oxygen.financial.service;

import info.froylanvillaverde.oxygen.financial.dao.PricelistDao;
import info.froylanvillaverde.oxygen.financial.model.Pricelist;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Froylan Villaverde Gonzalez
 * @since 0.0.2
 * @version 1.0
 */
public class PricelistServiceImpl implements PricelistService{
    
    @Autowired
    PricelistDao pricelistDao;
    
    @Override
    @Transactional(readOnly = true)
    public List<Pricelist> listPricelist() {
        return (List<Pricelist>)pricelistDao.findAll();
    }

    @Override
    @Transactional
    public void save(Pricelist pricelist) {
        pricelistDao.save(pricelist);
    }

    @Override
    @Transactional
    public void delete(Pricelist pricelist) {
        pricelistDao.delete(pricelist);
    }

    @Override
    @Transactional(readOnly = true)
    public Pricelist findPricelist(Pricelist pricelist) {
        return pricelistDao.findById(pricelist.getId()).orElse(null);
    }
    
}
