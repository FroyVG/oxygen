package info.froylanvillaverde.oxygen.financial.service;

import info.froylanvillaverde.oxygen.financial.model.Product;
import java.util.List;

/**
 *
 * @author Froylan Villaverde Gonzalez
 * @since 0.0.1
 * @version 1.0
 */
public interface ProductService {

    public List<Product> listProducts();
    
    public void save(Product product);
    
    public void delete(Product product);
    
    public Product findProduct(Product product);
}
