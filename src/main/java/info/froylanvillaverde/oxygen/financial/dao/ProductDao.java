package info.froylanvillaverde.oxygen.financial.dao;

import info.froylanvillaverde.oxygen.financial.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Froylan Villaverde Gonzalez
 * @since 0.0.1
 * @version 1.0
 */
public interface ProductDao extends JpaRepository<Product,Long>{
    
}
