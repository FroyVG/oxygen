package info.froylanvillaverde.oxygen.financial.dao;

import info.froylanvillaverde.oxygen.financial.model.Pricelist;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Froylan Villaverde Gonzalez
 * @since 0.0.2
 * @version 1.0
 */
public interface PricelistDao extends JpaRepository<Pricelist, Long>{
    
}
