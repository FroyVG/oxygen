package info.froylanvillaverde.oxygen.financial.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Froylan Villaverde Gonzalez
 * @since 0.0.1
 * @version 1.0
 */
@Entity
@Table(name = "stock", schema = "financial")
public class Stock {
    
    @Id
    @SequenceGenerator(name = "stock_id_seq", sequenceName = "stock_id_seq", schema = "financial", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "stock_id_seq")
    private Long id;
    
    @OneToOne(fetch = FetchType.LAZY,
            optional = false)
    @JoinColumn(name = "fk_product_id", nullable = false)
    private Product product;
    
    @Column
    @NotNull
    @Min(Long.MIN_VALUE)
    private Integer count;
    
    @Column
    private String location;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
    
    
    
}
