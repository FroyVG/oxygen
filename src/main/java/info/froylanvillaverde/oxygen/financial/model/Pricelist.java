package info.froylanvillaverde.oxygen.financial.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Froylan Villaverde Gonzalez
 * @since 0.0.2
 * @version 1.0
 */
@Entity
@Table(name = "pricelist", schema = "financial")
public class Pricelist {
    
    @Id
    @SequenceGenerator(name = "pricelist_id_seq", sequenceName = "pricelist_id_seq", schema = "financial", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pricelist_id_seq")
    private Long id;
    
    @Column
    @NotNull
    private Double price;
    
    @OneToOne(fetch = FetchType.LAZY,//Fetch the related entity lazily from the database 
            optional = false)
    @JoinColumn(name = "fk_product_id", nullable = false)//, updatable = false, nullable = false
    private Product product;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
        
    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @Override
    public String toString() {
        return "Pricelist{" + "id=" + id + ", price=" + price + "}";
    }
    
}
