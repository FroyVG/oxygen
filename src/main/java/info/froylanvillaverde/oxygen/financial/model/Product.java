package info.froylanvillaverde.oxygen.financial.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

/**
 *
 * @author Froylan Villaverde Gonzalez
 * @since 0.0.1
 * @version 1.4
 */
@Entity
@Table(name = "products", schema = "financial")
public class Product {
    
    @Id
    @SequenceGenerator(name = "products_id_seq", sequenceName = "products_id_seq", schema = "financial", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "products_id_seq")
    private Long id;
    
    @Column
    @NotEmpty
    private String key;
    
    @Column
    @NotEmpty
    private String name;
    
    @OneToOne(fetch = FetchType.LAZY,//Fetch the related entity lazily from the database
            cascade =  CascadeType.ALL,//Apply all cascading effects to the related entity. That is, whenever we update/delete a Product entity, update/delete the corresponding Pricelist as well.
            mappedBy = "product")//We use mappedBy attribute in the Product entity to tell hibernate that the Product entity is not responsible for this relationship and 
            //It should look for a field named product in the Pricelist entity to find the configuration for the JoinColumn/ForeignKey column.
    @Valid
    private Pricelist pricelist;
    
    @OneToOne(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            mappedBy = "product")
    @Valid
    private Stock stock;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public Pricelist getPricelist() {
        return pricelist;
    }

    public void setPricelist(Pricelist pricelist) {
        if (pricelist == null) {
            if (this.pricelist != null) {
                this.pricelist.setProduct(this);
            }
        }
        else {
            pricelist.setProduct(this);
        }
        this.pricelist = pricelist;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        if (stock == null) {
            if (this.stock != null) {
                this.stock.setProduct(this);
            }
        }
        else {
            stock.setProduct(this);
        }
        this.stock = stock;
    }
    
    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", key=" + key + ", name=" + name + "}";
    }
    
}
