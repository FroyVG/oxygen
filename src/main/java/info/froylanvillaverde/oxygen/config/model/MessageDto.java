package info.froylanvillaverde.oxygen.config.model;

/**
 *
 * @author Froylan Villaverde Gonzalez
 * @since 0.0.1
 * @version 1.0
 */
public class MessageDto {
    
    private String message;
    private Type type;

    public MessageDto(String message, Type type) {
        this.message = message;
        this.type = type;
    }
    
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public enum Type {
        
        DANGER("DANGER"),
        SUCCESS("SUCCESS"),
        INFO("INFO");
        
        private final String name;
        
        Type(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }
    
}
