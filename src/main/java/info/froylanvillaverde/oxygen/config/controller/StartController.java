package info.froylanvillaverde.oxygen.config.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 *
 * @author Froylan Villaverde Gonzalez
 * @since 0.0.1
 * @version 1.0
 */
@Controller
public class StartController {
    
    @GetMapping("/")
    public String start(){
        return "index";
    } 
    
}
